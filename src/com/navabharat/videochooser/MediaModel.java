package com.navabharat.videochooser;

public class MediaModel {

	public String url = null;
	public boolean status = false;

	public MediaModel(String url, boolean status) {
		this.url = url;
		this.status = status;
	}
}
