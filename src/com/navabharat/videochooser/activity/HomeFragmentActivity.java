package com.navabharat.videochooser.activity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.navabharat.videochooser.MediaChooser;
import com.navabharat.videochooser.MediaChooserConstants;
import com.navabharat.videochooser.R;
import com.navabharat.videochooser.fragment.VideoFragment;

public class HomeFragmentActivity extends FragmentActivity implements
		VideoFragment.OnVideoSelectedListener {

	private FragmentTabHost mTabHost;

	private static Uri fileUri;

	private final Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home_media_chooser);

		Log.d("HomeFragmentActivity", "onCreate() called");

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

		mTabHost.setup(this, getSupportFragmentManager(), R.id.realTabcontent);

		if (MediaChooserConstants.showVideo) {
			mTabHost.addTab(
					mTabHost.newTabSpec("tab2").setIndicator(
							getResources().getString(R.string.videos_tab)
									+ "      "), VideoFragment.class, null);
		}
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {

				android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
				VideoFragment videoFragment = (VideoFragment) fragmentManager
						.findFragmentByTag("tab1");
				android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();

				if (tabId.equalsIgnoreCase("tab2")) {

					if (videoFragment != null) {

						fragmentTransaction.show(videoFragment);
						if (videoFragment.getAdapter() != null) {
							videoFragment.getAdapter().notifyDataSetChanged();
						}
					}
					((TextView) (mTabHost.getTabWidget().getChildAt(0)
							.findViewById(android.R.id.title)))
							.setTextColor(Color.WHITE);
					((TextView) (mTabHost.getTabWidget().getChildAt(1)
							.findViewById(android.R.id.title)))
							.setTextColor(getResources().getColor(
									R.color.headerbar_selected_tab_color));
				}

				fragmentTransaction.commit();
			}
		});
	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
		}
	};

	/** Create a file Uri for saving an image or video */
	private Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type) {

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				MediaChooserConstants.folderName);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		if (type == MediaChooserConstants.MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else if (type == MediaChooserConstants.MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == MediaChooserConstants.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {

				sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
						fileUri));
				final AlertDialog alertDialog = MediaChooserConstants
						.getDialog(HomeFragmentActivity.this).create();
				alertDialog.show();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						// Do something after 5000ms
						String fileUriString = fileUri.toString()
								.replaceFirst("file:///", "/").trim();
						android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
						VideoFragment videoFragment = (VideoFragment) fragmentManager
								.findFragmentByTag("tab2");
						//
						if (videoFragment == null) {
							VideoFragment newVideoFragment = new VideoFragment();
							newVideoFragment.addItem(fileUriString);

						} else {
							videoFragment.addItem(fileUriString);
						}
						alertDialog.cancel();
					}
				}, 5000);

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the video capture
			} else {
				// Video capture failed, advise user
			}
		}
	}

	@Override
	public void onVideoSelected(int count) {
		if (count != 0) {
			//((TextView) mTabHost.getTabWidget().getChildAt(0)
			//		.findViewById(android.R.id.title)).setText(getResources()
			//		.getString(R.string.videos_tab) + "  " + count);
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			VideoFragment videoFragment = (VideoFragment) fragmentManager
					.findFragmentByTag("tab2");
			
			Intent videoIntent = new Intent();
			videoIntent.setAction(MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER );
			videoIntent.putStringArrayListExtra("list", videoFragment.getSelectedVideoList());
			sendBroadcast(videoIntent);
			finish();

		} else {
			((TextView) mTabHost.getTabWidget().getChildAt(0)
					.findViewById(android.R.id.title)).setText(getResources()
					.getString(R.string.video));
		}
	}

	public int convertDipToPixels(float dips) {
		return (int) (dips
				* HomeFragmentActivity.this.getResources().getDisplayMetrics().density + 0.5f);
	}

}
