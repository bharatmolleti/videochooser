package com.navabharat.videochooser.activity;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.navabharat.videochooser.MediaChooser;
import com.navabharat.videochooser.R;

public class TestActivity extends Activity {
	Button btnLauchVideoChooser = null;
	TextView textViewVideoInfo = null;
	
	BroadcastReceiver videoBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			Toast.makeText(TestActivity.this, "Video Selected",
					Toast.LENGTH_SHORT).show();
			List<String> filePathList = intent.getStringArrayListExtra("list");
			String filesList = "File : ";
			for (String fileName : filePathList) {
				Log.e("MainActivity", "File : " + fileName);
				filesList += fileName;
			}
			textViewVideoInfo.setText(filesList);
		}
	};
	
	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {

			Intent intent = new Intent(TestActivity.this,
					HomeFragmentActivity.class);
			startActivity(intent);

		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_test);
		
		btnLauchVideoChooser = (Button) findViewById(R.id.btn_LaunchVideoChooser);
		btnLauchVideoChooser.setOnClickListener(clickListener);
		
		textViewVideoInfo = (TextView) findViewById(R.id.textViewSelectedVideoName);
		
		IntentFilter videoIntentFilter = new IntentFilter(
				MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
		registerReceiver(videoBroadcastReceiver, videoIntentFilter);
		
	}
	

	@Override
	protected void onDestroy() {
		unregisterReceiver(videoBroadcastReceiver);
		super.onDestroy();
	}
}
